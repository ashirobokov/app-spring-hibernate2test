package ru.ashirobokov.java.app.dao;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.ashirobokov.java.app.model.BlogMessage;

import java.util.List;

@Repository
public class BlogDAOImpl implements BlogDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addMessage(BlogMessage message) {
        sessionFactory.getCurrentSession().save(message);
    }

    @Override
    public BlogMessage getMessage(Integer mesId) {
        BlogMessage message = (BlogMessage) sessionFactory.getCurrentSession().get(BlogMessage.class, mesId);

        return message;
    }

    @Override
    public void updateMessage(BlogMessage message) {
        sessionFactory.getCurrentSession().update(message);
    }

    @Override
    public void deleteMessage(Integer mesId) {
        BlogMessage mess = getMessage(mesId);
        if (mess != null)
            sessionFactory.getCurrentSession().delete(mess);

    }

    @Override
    public List<BlogMessage> getMessages() {
        return sessionFactory.getCurrentSession().createQuery("from BlogMessage").list();
    }

    @Override
    public List<BlogMessage> getMessages(String rubric) {
//        return sessionFactory.getCurrentSession()
//                .createQuery("FROM BlogMessage WHERE mesRubric LIKE :paramRubric")
//                .setParameter("paramRubric", rubric)
//                .list();
//
        return  sessionFactory.getCurrentSession().createCriteria(BlogMessage.class).add(Restrictions.eq("mesRubric", rubric)).list();
    }

    @Override
    public int getMessageCount(Integer mesId) {
        return ((Long) sessionFactory.getCurrentSession()
                .createQuery("select count (*) from BlogMessage where mesId = :id")
                .setInteger("id", mesId)
                .uniqueResult())
                .intValue();
    }

}
