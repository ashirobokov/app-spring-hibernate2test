package ru.ashirobokov.java.app.model;

import javax.persistence.*;

@Entity
@Table(name = "blog_message")
public class BlogMessage {

    @Id
 //   @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    private Integer mesId;

    @Column(name = "message_date")
    private String mesDate;

    @Column(name = "message_title")
    private String mesTitle;

    @Column(name = "message")
    private String blogMessage;

    @Column(name = "message_rubric")
    private String mesRubric;

    @Column(name = "message_link")
    private String mesLink;

    @Column(name = "message_url")
    private String mesUrl;


    public Integer getMesId() {
        return mesId;
    }

    public void setMesId(Integer mesId) {
        this.mesId = mesId;
    }

    public String getMesDate() {
        return mesDate;
    }

    public void setMesDate(String mesDate) {
        this.mesDate = mesDate;
    }

    public String getMesTitle() {
        return mesTitle;
    }

    public void setMesTitle(String mesTitle) {
        this.mesTitle = mesTitle;
    }

    public String getBlogMessage() {
        return blogMessage;
    }

    public void setBlogMessage(String blogMessage) {
        this.blogMessage = blogMessage;
    }

    public String getMesRubric() {
        return mesRubric;
    }

    public void setMesRubric(String mesRubric) {
        this.mesRubric = mesRubric;
    }

    public String getMesLink() {
        return mesLink;
    }

    public void setMesLink(String mesLink) {
        this.mesLink = mesLink;
    }

    public String getMesUrl() {
        return mesUrl;
    }

    public void setMesUrl(String mesUrl) {
        this.mesUrl = mesUrl;
    }

    @Override
    public String toString() {
        return "BlogMessage [mesId=" + mesId + ", mesDate=" + mesDate
                + ", mesTitle=" + mesTitle + ", blogMessage=" + blogMessage
                + ", mesRubric=" + mesRubric + ", mesLink=" + mesLink + ", mesUrl="
                + mesUrl + "]";
    }

}
