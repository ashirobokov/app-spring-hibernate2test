package ru.ashirobokov.java.app.service;

import ru.ashirobokov.java.app.model.BlogMessage;

import java.util.List;

public interface BlogService {

    public void addMessage(BlogMessage message);
    public BlogMessage getMessage(Integer mesId);
    public BlogMessage updateMessage(BlogMessage message);
    public void deleteMessage(Integer mesId);
    public List<BlogMessage> getMessages();
    public List<BlogMessage> getMessages(String rubric);
    public int getMessageCount(Integer mesId);

}
