package ru.ashirobokov.java.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ashirobokov.java.app.dao.BlogDAO;
import ru.ashirobokov.java.app.model.BlogMessage;

import java.util.List;

@Service
@Transactional
public class BlogServiceImpl implements BlogService {

    @Autowired
    private BlogDAO blogDAO;

    @Override
    public void addMessage(BlogMessage message) {
        blogDAO.addMessage(message);
    }

    @Override
    public BlogMessage updateMessage(BlogMessage message) {
        int messageId = message.getMesId();
        blogDAO.updateMessage(message);

        return blogDAO.getMessage(messageId);
    }

    @Override
    public BlogMessage getMessage(Integer mesId) {

    return blogDAO.getMessage(mesId);
    }

    @Override
    public void deleteMessage(Integer mesId) {
        blogDAO.deleteMessage(mesId);
    }

    @Override
    public List<BlogMessage> getMessages() {
        return blogDAO.getMessages();
    }

    @Override
    public List<BlogMessage> getMessages(String rubric) {
        return blogDAO.getMessages(rubric);
    }

    @Override
    public int getMessageCount(Integer mesId) {
        return blogDAO.getMessageCount(mesId);
    }
}
