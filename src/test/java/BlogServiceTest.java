import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import ru.ashirobokov.java.app.model.BlogMessage;
import ru.ashirobokov.java.app.service.BlogService;

import java.util.List;

@ContextConfiguration(locations = { "classpath:spring-app-test-context.xml" })
public class BlogServiceTest extends AbstractTransactionalTestNGSpringContextTests {

    @Autowired
    private BlogService blogService;

    private BlogMessage message;
    private Integer id;

    @BeforeTest(alwaysRun = true)
    public void setUp() {
        this.id = 101;
        message = new BlogMessage();
        message.setMesId(this.id);
        message.setMesDate("27.09.2017");
        message.setMesTitle("lorem lorem");
        message.setBlogMessage("lorem...lorem...lorem...lorem");
 //       message.setMesRubric("lorem");
        message.setMesRubric("VeryGoodLorem");
        message.setMesLink("lorem");
        message.setMesUrl("http:/www.lorem.com");
    }

    @Test(groups = "blog-service", enabled = true)
    @Rollback(false)
    public void testAddMessage() {
        blogService.addMessage(message);

        int count = blogService.getMessageCount(this.id);
        Assert.assertEquals(count, 1);
        System.out.println("FOUND MESSAGES (ADDED) = " + count);
    }

    @Test(groups = "blog-service", enabled = true)
    public void testGetMessage() {
        BlogMessage testMessage = blogService.getMessage(this.id);

        Assert.assertNotNull(testMessage.getMesId());
        System.out.println("GET MESSAGE = " + testMessage.toString());
    }


    @Test(groups = "blog-service", enabled = true)
    public void testGetMessages() {
        List<BlogMessage> messageList = blogService.getMessages();

        int messageNum = messageList.size();
        Assert.assertTrue(messageNum > 0);
        System.out.println("GET NUMBER OF MESSAGES = " + messageNum);
    }

    @Test(groups = "blog-service", enabled = true)
    public void testGetMessagesByRubric() {
        List<BlogMessage> messageList = blogService.getMessages(message.getMesRubric());

        int messageNum = messageList.size();
        Assert.assertTrue(messageNum > 0);
        System.out.println("GET NUMBER OF MESSAGES BY RUBRIC = " + messageNum);
    }

    @Test(groups = "blog-service", enabled = true)
    public void testUpdateMessage() {
        message.setMesTitle("Not quite lorem");
        BlogMessage testMessage = blogService.updateMessage(message);

        Assert.assertEquals(testMessage.getMesTitle(), "Not quite lorem");
        System.out.println("UPDATE FIELD Title : " + testMessage.getMesTitle());
    }

    @Test(groups = "blog-service", enabled = true)
    public void testDeleteMessage() {
        blogService.deleteMessage(this.id);

        int count = blogService.getMessageCount(this.id);
        Assert.assertEquals(count, 0);
        System.out.println("FOUND MESSAGES (DELETED) = " + count);
    }

    @AfterTest(alwaysRun = true)
    public void tearDown() {
        blogService.deleteMessage(this.id);
    }

}
